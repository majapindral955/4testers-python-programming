from simple_functions import is_adult
from simple_functions import get_words_containing_letter_a


def test_is_adult_for_age_greater_than_18():
    assert is_adult(19)  # == True


def test_is_adult_for_age_equal_18():
    assert is_adult(18)  # == True


def test_is_adult_for_age_less_than_18():
    assert is_adult(17) == False
    # assert not is_adult(17) (to samo co wyżej, inaczej zapisane)


def test_get_words_containing_letter_a():
    input_list = ["Papaya", "Cat", "Any", "Amazonia"]
    expected_list = ["Papaya", "Cat", "Any", "Amazonia"]
    assert get_words_containing_letter_a(input_list) == expected_list


def test_get_words_containing_mixed_words():
    input_list = ["Papaya", "bye", "Any", "Amazonia", "Miro", "why", "alabama"]
    expected_list = ["Papaya", "Any", "Amazonia", "alabama"]
    assert get_words_containing_letter_a(input_list) == expected_list

def test_empty_list():
    assert get_words_containing_letter_a([]) == []


def test_get_words_not_containing_letter_a():
    input_list = ["Bye", "Miro", "why"]
    expected_list = []
    assert get_words_containing_letter_a(input_list) == expected_list