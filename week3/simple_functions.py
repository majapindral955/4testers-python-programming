def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


# list_of_words = ['samolot', 'kot', 'armata', 'kalendarz', 'most']

def get_words_containing_letter_a(list_of_words):
    # return [word for word in list_of_words if ("a" in word) or ("A" in word)]
    return [word for word in list_of_words if "a" in word.lower()]

