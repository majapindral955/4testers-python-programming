if __name__ == '__main__':
    movies = ['Transporter', 'Stalker', 'Nothing Hill', 'Dune']
    movies_length = len(movies)
    print(f"The number of my favourite movies is {movies_length}")

    first_movie = movies[0]
    last_movie = movies[movies_length - 1]
    print(f"First movie: {first_movie}, Last movie: {last_movie}")

    # Add element at the end of a list
    movies.append("Star Wars")
    print(f"Last movie now: {movies[-1]}")

    # Replace element at index 1
    movies[1] = "Mr Bean"
    print(f"Favourite movies now: {movies}")

    movies.insert(1, "John Wick")
    print(f"Favourite movies now: {movies}")

    
