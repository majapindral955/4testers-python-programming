def company_email(name, surname):
    return f"{name.lower()}.{surname.lower()}@4testers.pl"

def multiplication_result(a, b, c):
    digit = 3
    return [digit*a, digit*b, digit*c]

def average_students_marks(marks_list):
    not_rounded_average = sum(marks_list) / len(marks_list)
    rounded_average = round(not_rounded_average, 2)
    return rounded_average


if __name__ == '__main__':

    # Exercise 1 - lists
    emails = ["a@example.com", "b@example.com"]
    emails_length = len(emails)
    print(f"The number of emails is {emails_length}")

    first_email = emails[0]
    last_email = emails[emails_length - 1]
    print(f"First email: {first_email}, Last email: {last_email}")

    emails.append("cde@example.com")
    print(f"Emails now: {emails}")

    # Exercise 2
    email1 = company_email("Janusz", "Nowak")
    email2 = company_email("Barbara", "Kowalska")
    print(email1)
    print(email2)

    # Exercise 3
    number1 = multiplication_result(4, 5, 8)
    print(number1)

    # Exercise 4
    student_marks_a = [5, 3, 2, 4, 1, 1, 3, 5, 4]
    average_of_students_mark = average_students_marks(student_marks_a)
    print(f"Student a grade point is:", average_students_marks(student_marks_a))