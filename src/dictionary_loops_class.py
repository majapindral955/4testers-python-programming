import random
import string


def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)
    return result_str


def get_random_login_data_for_email(email):
    return {
        "email": email,
        "password": get_random_string(12)

    }


if __name__ == '__main__':
    friend = {
        "name": "Paweł",
        "age": 32,
        "hobbies": ["motorcycle", "sport"]
    }
    friend["name"] = "Bartosz"
    friend["age"] = friend["age"] + 1
    friend["age"] += 2
    friend["hobbies"].append("archery")

    print(friend)

# Exercise 1
new_login_data = get_random_login_data_for_email("maja@exampe.com")
print(new_login_data)
