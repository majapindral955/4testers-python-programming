import random
import datetime

# Przykładowe dane
female_names = ['Anna', 'Maria', 'Katarzyna', 'Małgorzata', 'Agnieszka']
male_names = ['Jan', 'Andrzej', 'Paweł', 'Tomasz', 'Krzysztof']
last_names = ['Nowak', 'Kowalski', 'Wiśniewski', 'Dąbrowski', 'Lewandowski']
countries = ['Poland', 'Germany', 'France', 'Spain', 'Italy']


def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


# Funkcja do generowania słownika
def generate_dictionary(is_female):
    if is_female:
        first_name = random.choice(female_names)
    else:
        first_name = random.choice(male_names)
    last_name = random.choice(last_names)
    email = f"{first_name.lower()}.{last_name.lower()} @ example.com"

    current_year = datetime.datetime.now().year
    age = random.randint(5, 45)
    birth_year = current_year - age
    is_adult = age >= 18
    country = random.choice(countries)

    return {
        "first_name": first_name,
        "last_name": last_name,
        "email": email,
        "age": age,
        "birth_year": birth_year,
        "is_adult": is_adult,
        "country": country
    }


# 2. Funkcja do wygenerowania słowników

def generate_lists_of_dictionaries(number_of_male, number_of_female):
    output_list = []
    for i in range(number_of_male):
        output_list.append(generate_dictionary(False))
    for i in range(number_of_female):
        output_list.append(generate_dictionary(True))
        return output_list


def generate_description_for_person_dictionary(person_dictionary):
    return f"Hi! I'm {person_dictionary['first_name']} {person_dictionary['last_name']}. I come from {person_dictionary['country']} and I was born in {person_dictionary['birth_year']}"


if __name__ == '__main__':
    random_people_list = generate_lists_of_dictionaries(5, 5)
    print(random_people_list)
    for element in random_people_list:
        print(generate_description_for_person_dictionary(element))
