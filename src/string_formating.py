def city_greeter(name, city):
    greeting = f"Witaj {name.upper()}! Miło Cię widzieć w naszym mieście: {city.upper()}!"
    print(greeting)


if __name__ == '__main__':
    # Exercise 1
    first_name = "Maja"
    last_name = "Kuc"
    email = first_name.lower() + "." + last_name.lower() + "@4testers.pl"
    print(email)

    email_formated = f"{first_name.lower()}.{last_name.lower()}@4testers.pl"
    print(email_formated)
    # Exercise 2
    city_greeter("Michał", "Toruń")
    city_greeter("Beata", "Gdynia")
