def check_if_standard_conditions(temp_in_celsius, pressure_in_hpa):
    if temp_in_celsius == 0 and pressure_in_hpa == 1013:
        return True
    else:
        return False


def get_grade_for_test_score(test_score):
    if test_score >= 90:
        return 5
    elif test_score >= 75:
        return 4
    elif test_score >= 50:
        return 3
    else:
        return 2


def print_promotions_for_age(age):
    if age >= 65:
        print("You have 70% off")
    elif age >= 50:
        print("You have 50% off")
    elif age >= 30:
        print("You have 25% off")
    else:
        print("You don't have any promo")


if __name__ == '__main__':
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(0, 1013)}")
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(0, 1014)}")
    print(f"Checking if standard conditions for 0 Celsius, 1013 hPa {check_if_standard_conditions(1, 1014)}")

    print(f"Grade fo 99% is {get_grade_for_test_score(99)}")
    print(f"Grade fo 99% is {get_grade_for_test_score(90)}")
    print(f"Grade fo 99% is {get_grade_for_test_score(89)}")

    # Exercise_age

    print_promotions_for_age(66)
    print_promotions_for_age(65)
    print_promotions_for_age(64)
    print_promotions_for_age(61)
    print_promotions_for_age(51)
    print_promotions_for_age(50)
    print_promotions_for_age(43)
    print_promotions_for_age(43)
    print_promotions_for_age(25)
