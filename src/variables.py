friend_first_name = "Paweł"
friend_age = 31
friend_number_of_pets = 2
friend_has_driving_licence = True
friendship_length_in_years = 8.5

print("My friend's first name:", friend_first_name)
print("My friend's age:", friend_age)
print("My friend's number of pets:", friend_number_of_pets)
print("My friend has driving licence:", friend_has_driving_licence)
print("Friendship length in years:", friendship_length_in_years)
